const setQuery = function(answer, nextPath) {
  const query = answer === 'yes' ? '1' : '0'

  let exRoute
  if (this.$route.query.route) {
    exRoute = this.$route.query.route
  } else {
    exRoute = ''
  }

  return nextPath + `/?route=${exRoute}${query}`
}

export default ({ app }, inject) => {
  inject('setQuery', setQuery)
}
